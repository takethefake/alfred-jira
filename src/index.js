import AlfredNode from 'alfred-workflow-nodejs';
import jiraExt from './jira';

const actionHandler = AlfredNode.actionHandler;
const workflow = AlfredNode.workflow;
workflow.setName('example-alfred-workflow-nodejs');
const Item = AlfredNode.Item;

(function main() {
  // --- simple example of using action handler
  actionHandler.onAction('action1', () => {
    const jql =
      'assignee = currentUser() ' +
      "AND status in (Resolved, Closed, 'Ready for Review', 'Ready for Testing') " +
      'AND updatedDate >= startOfDay()' +
      ' ORDER BY updatedDate DESC';
    jiraExt
      .searchByJQL(jql)
      .then(
        items =>
          new Promise(success => {
            items.forEach(item => {
              workflow.addItem(item);
            });
            success(workflow);
          }),
      )
      .then(feedbackWorkflow => feedbackWorkflow.feedback());
  });

  // --- example of menu handler
  // handle action "menuExample"
  actionHandler.onAction('menuExample', () => {
    const item1 = new Item({
      title: 'Feedback A',
      subtitle: 'Press tab to get menu items',
      arg: 'Feedback A',
      hasSubItems: true, // set this to true to tell that this feedback has sub Items
      valid: true,
      data: { alias: 'X' }, // we can set data to item to use later to build sub items
    });

    workflow.addItem(item1);

    const item2 = new Item({
      title: 'Feedback B',
      subtitle: 'Press tab to get menu items',
      arg: 'Feedback B',
      hasSubItems: true, // set this to true to tell that this feedback has sub Items
      valid: true,
      data: { alias: 'Y' }, // we can set data to item to use later to build sub items
    });

    workflow.addItem(item2);

    // generate feedbacks
    workflow.feedback();
  });

  // handle menu item select of action "menuExample"
  actionHandler.onMenuItemSelected('menuExample', (query, title, data) => {
    const item1 = new Item({
      title: `Item 1 of ${title}`,
      arg: `item 1 of ${title} which has alias ${data.alias}`,
      subtitle: data.alias,
      valid: true,
    });

    const item2 = new Item({
      title: `Item 2 of ${title}`,
      arg: `item 2 of ${title} which has alias ${data.alias}`,
      subtitle: data.alias,
      valid: true,
    });

    workflow.addItem(item1);
    workflow.addItem(item2);
    // generate feedbacks
    workflow.feedback();
  });

  AlfredNode.run();
})();
