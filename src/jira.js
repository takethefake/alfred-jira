import JiraApi from 'jira-client';
import AlfredNode from 'alfred-workflow-nodejs';

const Item = AlfredNode.Item;

export const jira = new JiraApi({
  protocol: 'https',
  host: 'jira.incloud.zone',
  username: '',
  password: '',
  apiVersion: '2',
  strictSSL: true,
});

const config = {
  searchByJQL: jql =>
    new Promise(success => {
      jira
        .searchJira(jql, {
          maxResults: 9,
          fields: ['summary', 'description', 'issuetype', 'key'],
        })
        .then(result => success(config.issuesToAlfredItems(result.issues)))
        .catch(apiError => {
          config.apiErrorToAlfredItem(apiError);
        });
    }),

  issuesToAlfredItems: issues =>
    new Promise(success => {
      if (issues.length === 0) {
        const item = new Item({
          title: 'No Results',
          subtitle: '',
          arg: '',
          hasSubItems: false, // set this to true to tell that this feedback has sub Items
          valid: true,
        });
        success([item]);
      }
      const issueItems = issues.map(
        issue =>
          new Item({
            title: `${issue.key} ${issue.fields.summary}`,
            subtitle: issue.fields.description,
            arg: '',
            hasSubItems: false, // set this to true to tell that this feedback has sub Items
            valid: true,
          }),
      );
      success(issueItems);
    }),

  apiErrorToAlfredItem: apiError =>
    new Promise(success => {
      console.log(apiError);
      const errorItem = new Item({
        title: 'apiError',
        subtitle: 'error description',
        arg: '',
        hasSubItems: false, // set this to true to tell that this feedback has sub Items
        valid: true,
      });
      success(errorItem);
    }),
};

export default config;
